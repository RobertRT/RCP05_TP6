package tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.*;


public class ChatServeur extends JFrame implements ActionListener {

	/*___________ Attributs ______________*/
	private JTextArea messageBoard;		//Zone d'affichage
	private JButton b_Exit;				//Bouton exit
	private ServerSocket serveur;		
	private int portNumber = 8888;
	private PrintWriter WritterServeur;

	/*___________ Constructeur ______________*/
	public ChatServeur(){
		//D�finition de la fen�tre
		super("Serveur - Panneau d'affichage");
		setSize(400, 300);
		setLocation(200,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageBoard = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(messageBoard, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		messageBoard.setEditable(false);
		b_Exit = new JButton("Exit");
		b_Exit.addActionListener(this); 

		// Ajout des composants graphique � la fen�tre
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(scrollPane, BorderLayout.CENTER);
		pane.add(b_Exit, BorderLayout.SOUTH);

		//Affichage d'un message de bienvenue
		afficherPanneau("Le panneau est actif\n");

		//Cr�ation du ServeurSocket
		try {
			serveur = new ServerSocket(portNumber);
			afficherPanneau("Serveur d�marr�\n");

		} catch (Exception e) {
			afficherPanneau("Erreur de cr�ation ServerSocket \n");
		}

		//Affichage de la fen�tre
		setVisible(true);

		// D�mmarage de l'�coute 
		ecouter();				
	}	


	/*___________ M�thodes ______________*/

	// D�marrage de l'�coute
	private void ecouter() {

		try {
			// Attente de connexion. Bloqu� jusqu'� ce qu'une connexion soit faite
			afficherPanneau("Serveur en attente de connexion...\n");

			//Sortie de l'attente - Cr�ation d'une socket pour chaque client connect�
			Socket clientConnecte = serveur.accept();
			afficherPanneau("Client connect�\n");
			WritterServeur = new PrintWriter(clientConnecte.getOutputStream());
			WritterServeur.println("Bienvenue"); //Envoi du texte par l'objet writer
			WritterServeur.flush();

			//Les messages re�us sont format�s et stock�s dans une m�moire tampon 
			BufferedReader readerServeur = new BufferedReader(new InputStreamReader(clientConnecte.getInputStream()));

			//On lit et affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerServeur.readLine()) != null){ 
				afficherPanneau("Message recu : " + ligne + "\n");
				WritterServeur.println(ligne); //Envoi du texte par l'objet writer
				WritterServeur.flush();
				
			}

		} catch (Exception e) {
			afficherPanneau("Erreur dans le traitement de la connexion");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Fermeture de l'application sur action du bouton Exit 
		if (e.getSource() == b_Exit) {
			System.exit(-1);
		}
	}
	
	public void afficherPanneau(String str){
		messageBoard.append(str);
	}

	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatServeur();

	}

}
