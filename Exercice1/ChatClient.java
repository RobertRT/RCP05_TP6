package tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener,Runnable {

	/*___________ Attributs ______________*/
	private JTextField messageSortant;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;					//Socket du programme client
	private int portNumber = 8888;			//Port 
	private String serverAddr = "localhost";//Adresse du serveur
	private PrintWriter writerClient;				
	private BufferedReader readerClient;
	/*___________ Constructeur ______________*/
	public ChatClient(){
		//D�finition de la fen�tre
		super("Client - Panneau d'affichage");
		setSize(250, 120);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageSortant = new JTextField(20);
		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);

		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new FlowLayout());
		pane.add(messageSortant);
		pane.add(b_Envoyer);

		//Cr�ation de la Socket client
		try {
			maSocket = new Socket(serverAddr, portNumber);
			writerClient = new PrintWriter(maSocket.getOutputStream());

			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));


		} catch (Exception e) {
			System.out.println("Erreur Cr�ation client");
		}

		//Affichage de la fen�tre
		setVisible(true);
		
	}

	/*___________ M�thodes ______________*/
	@Override
	public void actionPerformed(ActionEvent e) {
		//Clic du bouton Envoyer d�clanche la m�thode emettre()
		if (e.getSource() == b_Envoyer) {
			emettre();
			Thread processus = new Thread(this);
			processus.start();
			

		}

	}

	public void emettre() {
		//Envoi du message
		try {
			String messageAEnvoyer = messageSortant.getText(); 
			writerClient.println(messageAEnvoyer); //Envoi du texte par l'objet writer
			writerClient.flush();
			messageSortant.setText("");
			messageSortant.requestFocus();


		} catch (Exception e) {
			System.out.println("Erreur Envoi du Message");
		}
	}
	public void ecouter() {
		


		try {



			//On lit et affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerClient.readLine()) != null){ 
				System.out.println("Message recu : " + ligne + "\n");


			}

		} catch (Exception e) {
			System.out.println("Erreur dans le traitement de la connexion");
		}
	}


	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatClient();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		ecouter();
	}




}
